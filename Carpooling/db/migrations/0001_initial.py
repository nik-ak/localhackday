# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-26 16:08
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('user_id', models.IntegerField(primary_key=True, serialize=False)),
                ('full_name', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=254)),
                ('notification_frequency', django.contrib.postgres.fields.jsonb.JSONField()),
            ],
        ),
    ]
