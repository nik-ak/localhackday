# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.validators import validate_email


class Notification(models.Model):
    user_id = models.CharField(max_length=36, primary_key=True)
    full_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, unique=True)
    notification_frequency = models.CharField(max_length=10)#JSONField()

    def __str__(self):
        row = self.user_id+";"+self.full_name+";"+self.email+";"+str(self.notification_frequency)
        return row
