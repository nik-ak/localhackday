# from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.staticfiles.templatetags.staticfiles import static
# from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie

import modules.search.blablacar
import modules.search.prevoz
import modules.notify

import json

fake_response = {
    "from":"Koper",
    "to":"Ljubljana",
    "departure":'2016-12-13',
    "ETA":'1231',
    "URL":"http://google.com",
    "price":"5$"
}

@ensure_csrf_cookie
def index(request):
    return render(request, 'index.html', {'STATIC_URL':static('')})

@ensure_csrf_cookie
def findRides(request):
    if request.path == '/getRides/':
        if request.method == 'POST':
            req = request.read().decode('utf-8')
            results = []
            s_bla, res_bla = modules.search.blablacar.fetch_blabla(req)
            s_pr, res_pr = modules.search.prevoz.fetch_prevoz(req)
            if s_bla and s_pr:
                ret = {"status":1,"content":{}, "errors":[res_bla,res_pr]}
            elif s_bla and not(s_pr):
                ret = {"status":1,"content":res_pr, "errors":res_bla}
            elif s_pr and not(s_bla):
                ret = {"status":1,"content":res_bla, "errors":res_pr}
            else:
                ret = {"status":0,"content":res_bla+res_pr}
            return JsonResponse(ret, safe=False)
        else:
            index(request)
    else:
        index(request)

@ensure_csrf_cookie
def getNotifications(request):
    if request.path == '/getNotifications/':
        if request.method == 'POST':
            req = json.loads(request.body.decode("utf-8"))
            modules.notify.notification(req)
            return JsonResponse({"status":"200"},safe=False)
    index(request)
