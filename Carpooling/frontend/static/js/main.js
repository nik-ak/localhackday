window.onload=function () {
	console.log("loaded in")
	var id=[
		"notification","notificationemail","notificationsubmit","notificationdiv","notificationclosediv","notificationclose",
		"city1","city2","dateafter","dateuntil","find",
		"results","results-body","indicator",
		"rides","torides","tours","totours"]
	window.DOM={}
	for(var x in id)
		window.DOM[id[x]]=document.getElementById(id[x])
	DOM.find.onclick=find
	DOM.notification.onclick=showNotification
	DOM.notificationclose.onclick=hideNotification
	DOM.notificationsubmit.onclick=submitNotification
	
	DOM.torides.onclick=toRides
	DOM.totours.onclick=toTours
	setup()
}

var csrftoken = Cookies.get('csrftoken');

window.onkeyup=function (e) {
	//if(e.keyCode==13) find()
	//if(e.keyCode==27) hideNotification()
}

function leadingZeros(i) {
	return (i<10?"0":"")+i
}
function setup() {
	sorttable.sort_alpha = function(a,b) { return a[0].localeCompare(b[0]); }

	DOM.AfterCalendar=new dhtmlXCalendarObject({input:"dateafter",enableTime:true})
	DOM.AfterCalendar.setDateFormat("%Y/%m/%d %H:%i");
	DOM.AfterCalendar.setDate(new Date())
	DOM.dateafter.value=DOM.AfterCalendar.getFormatedDate()
	DOM.AfterCalendar.attachEvent("onTimeChange",function(d) {
		DOM.dateafter.value=DOM.AfterCalendar.getFormatedDate()
	})
	DOM.dateafter.onchange=function () {
		DOM.AfterCalendar.setDate(DOM.dateafter.value)
	}

	DOM.UntilCalendar=new dhtmlXCalendarObject({input:"dateuntil",enableTime:true})
	DOM.UntilCalendar.setDateFormat("%Y/%m/%d %H:%i");
	var d=new Date()
	d.setDate(d.getDate()+1)
	DOM.UntilCalendar.setDate(d)
	DOM.UntilCalendar.attachEvent("onTimeChange",function(d) {
		DOM.dateuntil.value=DOM.UntilCalendar.getFormatedDate()
	})
	DOM.dateuntil.value=DOM.UntilCalendar.getFormatedDate()
	DOM.dateuntil.onchange=function () {
		DOM.UntilCalendar.setDate(DOM.dateuntil.value)
	}

	autocomplete1=new google.maps.places.Autocomplete(DOM.city1)
	autocomplete2=new google.maps.places.Autocomplete(DOM.city2)
	
	toRides()
}

function JStoParams(o) {
	var s=""
	for(var x in o) {
		s+=x+"="+o[x]+"&"
	}
	s=s.substring(0, s.length-1)
	return s
}

function find() {

	var DateAfter=DOM.AfterCalendar.getDate()

	var DateUntil=DOM.UntilCalendar.getDate()

	body=JStoParams({
			fn:DOM.city1.value,
			tn:DOM.city2.value,
			db:DateAfter.getTime(),
			de:DateUntil.getTime()
		})
	console.log(body.length)
	headers = new Headers()
	headers.append("content-length",body.length)
	headers.append("content-type","application/x-www-form-urlencoded")
	headers.append("X-CSRFToken", csrftoken)
	resetResults();
	showIndicator();
	fetch("./getRides/",{
		method:"POST",
		body:(body),
		headers:headers,
		credentials:'same-origin'
	})
	.then(function(response) {
		if(response.status!=200) {
			console.log("<<<=====ERROR====>>>")
			console.log(response)
			return
		}
		console.log(response)
		hideIndicator()
		response.text().then(update)
	})
}

function submitNotification() {
	//hideNotification()
	body=JSON.stringify({
			email:DOM.notificationemail.value
		})
	console.log(body.length)
	headers=new Headers()
	headers.append("content-length",body.length)
	headers.append("content-type","text/json")
	headers.append("X-CSRFToken", csrftoken)
	fetch("./getNotifications/",{
		method:"POST",
		body:body,
		headers:headers,
		credentials:'same-origin'
	})
	.then(function(response) {
		if(response.status!=200) {
			console.log("<<<=====ERROR====>>>")
			console.log(response)
			return
		}
		console.log(response)
		hideNotification()
	})
}

function update(data){
	var p=JSON.parse(data)
	console.log(p)
	var w=p.content
	resetResults()
	for(var x in w) {
		var tr=document.createElement('tr')
		var td=document.createElement('td')
		td.innerHTML="<a href=\"http://google.com/maps/place/"+encodeURIComponent(w[x].fn)+"\" target=_blank>"+w[x].fn+"</a>"
		tr.appendChild(td)
		var td=document.createElement('td')
		td.innerHTML="<a href=\"http://google.com/maps/place/"+encodeURIComponent(w[x].tn)+"\" target=_blank>"+w[x].tn+"</a>"
		tr.appendChild(td)
		var td=document.createElement('td')
		var d=new Date(w[x].dd)
		td.innerHTML=d.getFullYear()+"/"+leadingZeros(d.getMonth()+1)+"/"+leadingZeros(d.getDate())+" "+leadingZeros(d.getHours())+":"+leadingZeros(d.getMinutes())
		td.setAttribute("sorttable_customkey",d.getTime())
		tr.appendChild(td)
		var td=document.createElement('td')
		if(typeof w[x].ad == "number") {
			var d=new Date(w[x].ad)
			td.innerHTML=d.getFullYear()+"/"+leadingZeros(d.getMonth()+1)+"/"+leadingZeros(d.getDate())+" "+leadingZeros(d.getHours())+":"+leadingZeros(d.getMinutes())
			td.setAttribute("sorttable_customkey",d.getTime())
		}
		else {
			td.innerHTML=w[x].ad
			td.setAttribute("sorttable_customkey",Number.MAX_SAFE_INTEGER)
		}
		tr.appendChild(td)
		var td=document.createElement('td')
		td.innerHTML="<a href=\""+w[x].url+"\" target=_blank>"+w[x].source+"</a>"
		tr.appendChild(td)
		var td=document.createElement('td')
		td.innerHTML=w[x].price
		tr.appendChild(td)
		DOM["results-body"].appendChild(tr)
		var departureth = DOM.results.getElementsByTagName("th")[2]
		var arrivalth = DOM.results.getElementsByTagName("th")[3]
		sorttable.innerSortFunction.apply(arrivalth, [])
		sorttable.innerSortFunction.apply(departureth, [])
	}
}

function resetResults() {
	DOM["results-body"].innerHTML=""
}

function parseDate(s) {
	var t=s.split(" ")
	t[0]=t[0].split(/[\.\/]/)
	t[1]=t[1].split(":")
	var d=new Date()
	d.setDate(t[0][0])
	if(d.getMonth()>t[0][1]-1) d.setFullYear(d.getFullYear+1)
	d.setMonth(t[0][1]-1)
	d.setHours(t[1][0])
	d.setMinutes(t[1][1])
	return d
}

function toRides() {
	DOM.tours.style.display="none"
	DOM.rides.style.display="table"
}
function toTours() {
	DOM.tours.style.display="table"
	DOM.rides.style.display="none"
}

function showNotification() {
	DOM.notificationdiv.style.display="block"
}

function hideNotification() {
	DOM.notificationdiv.style.display="none"
}

function showIndicator() {
	DOM.indicator.style.display="table"
}

function hideIndicator() {
	DOM.indicator.style.display="none"
}
