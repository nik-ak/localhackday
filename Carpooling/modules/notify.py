from uuid import uuid4
from django.core.exceptions import ValidationError
# from .send_email import send_email
from .cronjob import *

# Import database for notifications
from db.models import Notification

def fake_user(email):
    user = {}
    user['user_id'] = str(uuid4())
    user['full_name'] = "Name Surname"
    user['email'] = email
    user['notification_frequency'] = '5 min'
    return user

def notification(req):
    try:
        user = fake_user(req['email'])
        obj = Notification(**user)
        obj.full_clean()
        obj.save()
        add_job(init_job(user))
    except ValidationError as e:
        print(e)
