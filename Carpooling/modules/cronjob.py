from django.conf import settings
import subprocess

CRONTAB_PATH = settings.CRONTAB_PATH
EMAIL_SENDER_SCRIPT = settings.EMAIL_SENDER_SCRIPT
BASE_DIR = settings.BASE_DIR
CRONTAB_FILE_PATH = settings.CRONTAB_FILE_PATH

# args - data from database: user_id, full_name, email, notification_frequency
def init_job(args):
    job = interpret_frequency(args['notification_frequency'])+' '
    job += EMAIL_SENDER_SCRIPT+' '+args['full_name']+' '+args['email']+'\n'
    return job

def load_cron_jobs():
    command = CRONTAB_PATH+' '+CRONTAB_FILE_PATH
    subprocess.Popen(command.split())


# frequencies: 10 min, 30 min, 1 hour, 1 day, 1 week
def interpret_frequency(freq):
    return {
    '5 min' : '*/5 * * * *', # every 5 minutes
    '10 min' : '*/10 * * * *', # every 10 minutes
    '30 min' : '*/30 * * * *', # every 30 minutes
    '1 hour' : '* */1 * * *', # every hour
    '1 day' : '0 10 * * *', # every day at 10 am
    '1 week' : '0 10 * * 1' # every monday at 10 am
    }[freq]

# job is a string in crontab format, \n is needed at the end of the string
def add_job(job):
    with open(CRONTAB_FILE_PATH,'a') as f:
        f.write(job)
    load_cron_jobs()

def read_active_jobs():
    command = CRONTAB_PATH+' -l'
    return subprocess.Popen(command.split(),stdout=subprocess.PIPE).communicate()[0].decode('utf-8')

def stop_active_jobs():
    command = CRONTAB_PATH+' -r'
    subprocess.Popen(command.split())

def remove_job(job):
    with open(CRONTAB_FILE_PATH,'r+') as f:
        cur_jobs = f.read()
        if job in cur_jobs:
            new_jobs = cur_jobs.replace(job,'')
            f.seek(0)
            f.write(new_jobs)
            f.truncate()
            load_cron_jobs()
            return True
        else:
            return False
