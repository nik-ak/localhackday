import json
import datetime
import time
import requests

default = {
	'locale':'en_GB',
	'_format':'json',
	'cur':'EUR',
	'page': 1,
	'sort':'trip_date',
	'order':'asc'
}

headers = {
	'accept': "application/json",
	'key' : "c87d4a06fd5444819f83072d8ca4fdbe"
}

def refine_request_and_map_to_dict(p):
	args = json.loads(p)
	args['db'] = datetime.datetime.fromtimestamp(args['db']/1000).strftime("%Y-%m-%d %H:%M:%S")
	args['de'] = datetime.datetime.fromtimestamp(args['de']/1000).strftime("%Y-%m-%d %H:%M:%S")
	return args

def str_to_coef(s):
	if s=='':
		s='s'
	return {
		's':1000,
		'ms':1,
		'm':60000,
		'h':60*60000,
		'd':24*60*60000
	}[s]

def fetch_blabla(parameters):
	status = 0
	try:
		parameters = refine_request_and_map_to_dict(parameters)
	except:
		status = 1
		print("got here")
		return status, {"error_message":"Date can't be parsed."}
	url = "http://public-api.blablacar.com/api/v2/trips"
	ret=[]
	def_params = default.copy()
	while(True):
		r = requests.get(url,params={**def_params,**parameters},headers=headers)
		if (r.status_code == 404 and def_params['page'] == 1):
			status = 1
			break
		elif (r.status_code == 404 and def_params['page'] != 1):
			status = 0
			break
		elif (r.status_code != 200):
			status = 1
			return status, {"error_message":"Server error: 'GET' request from blablacar.com has failed."}
		f = r.json()['trips']
		for i in f:
			t={}
			t['fn']=i['departure_place']['city_name']
			t['tn']=i['arrival_place']['city_name']
			t['dd']=int(time.mktime(datetime.datetime.strptime(i['departure_date'],"%d/%m/%Y %H:%M:%S").timetuple())*1000)
			t['ad']=t['dd']+int(i['duration']['value'])*str_to_coef(i['duration']['unity'])
			t['url']=i['links']['_front']
			t['source']="BlaBlaCar"
			t['price']=i['price']['string_value'][1:].split('.')[0]+"€"
			ret.append(t)
		def_params['page']+=1
	return status, ret
