import json
import datetime
import time
import requests

# my_key
google_api_key = "AIzaSyC8I97nlgj3I7SEhNoV41GQkxKExroqYQY"
# not my key :)
# google_api_key = "AIzaSyAJ3Fc4HM2RrMtjsAyzx4HlkvxGgpLghe4"



def refine_request(args_in):
    args = args_in.copy()
    args['db'] = datetime.datetime.fromtimestamp(args['db']/1000).strftime("%Y-%m-%d %H:%M:%S")
    args['de'] = datetime.datetime.fromtimestamp(args['de']/1000).strftime("%Y-%m-%d %H:%M:%S")
    return args

def find_normal_name(args,flag):
    url = 'https://maps.googleapis.com/maps/api/geocode/json'
    query = {"address":args[flag],"key":google_api_key}
    r = requests.get(url,params=query)
    if (r.status_code != 200):
        raise ValueError("Server error: 'GET' request for names of cities has failed.")
    obj_json = r.json()
    try:
        obj_json = obj_json['results'][0]['address_components']
    except:
        raise ValueError("Server error: 'GET' request for names of cities has failed.")
    name = ""
    country_name = ""
    for i in obj_json:
        l = i['types']
        for j in l:
            if "locality" in l:
                name = i['long_name']
                break
            elif "sublocality" in l:
                name = i['long_name']
            elif "country" in l:
                country_name = i["short_name"]
    return name,country_name

def time_from_iso8601_to_timestamp(t):
    temp = t.split(":")[:2]
    return int(time.mktime(datetime.datetime.strptime(temp[0]+":"+temp[1],"%Y-%m-%dT%H:%M").timetuple())*1000)

def find_duration(args):
    url_google = "https://maps.googleapis.com/maps/api/distancematrix/json"
    query = {"units":"metric", "origins":args['fn'],"destinations":args['tn'],"key":google_api_key}
    r = requests.get(url_google,params=query)
    ad_json = r.json()
    return ad_json['rows'][0]['elements'][0]['duration']['value']*1000

def check_price(p):
    if p == None:
        return "Not listed"
    else:
        return str(p)+"€"

def fetch_prevoz(parameters):
    jparameters = json.loads(parameters)
    status = 0
    try:
        request_dict = refine_request(jparameters)
    except:
        status = 1
        return status, {"error_message":"Date can't be parsed."}
    date_begin,time_begin = request_dict['db'].split(' ')
    date_end,time_end = request_dict['de'].split(' ')

    try:
        fn,fcn = find_normal_name(request_dict,'fn')
        tn,tcn = find_normal_name(request_dict,'tn')
    except ValueError as err:
        status = 1
        return status, {"error_message":err.args[0]}

    if fn == "" or tn == "":
        status = 1
        return status, {"error_message":"Unknown name of the city."}

    # Determine if the request is within Slovenia
    internal = False
    if fcn == "SI" and tcn == "SI":
        internal = True

    if (internal):
        url = "http://prevoz.org/api/search/shares/"
    else:
        url = "http://prevoz.org/api/search/international/"


    d = datetime.datetime.strptime(date_begin,"%Y-%m-%d")
    d_end = datetime.datetime.strptime(date_end,"%Y-%m-%d")
    res_dict_list = []
    while d<=d_end:
        if (internal):
            query = {"f":fn,"t":tn,"d":d.strftime("%Y-%m-%d")}
        else:
            query = {"fc":fcn,"tc":tcn,"d":d.strftime("%Y-%m-%d")}
        r = requests.get(url,params=query)
        if (r.status_code!=200):
            status = 1
            return status, {"error_message":"Server error: 'GET' request from prevoz.com has failed."}
        res_dict_list += r.json()['carshare_list']
        d += datetime.timedelta(days=1)

    ret = []

    estimated_arrival = find_duration(request_dict)
    for i in res_dict_list:
        dd_timestamp = time_from_iso8601_to_timestamp(i['date_iso8601'])
        if dd_timestamp >= jparameters['db'] and dd_timestamp <= jparameters['de']:
            d = {}
            if (internal):
                d['fn'] = i['from']
                d['tn'] = i['to']
            else:
                d['fn'] = i['from_country_name']
                d['tn'] = i['to_country_name']
            d['dd'] = time_from_iso8601_to_timestamp(i['date_iso8601'])
            d['ad'] = d['dd']+estimated_arrival
            d['url'] = "http://prevoz.org/prevoz/view/"+str(i['id'])
            d['price'] = check_price(i['price'])
            d['source'] = "Prevoz"
            ret.append(d)
    return status, ret
