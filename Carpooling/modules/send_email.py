#!/Library/Frameworks/Python.framework/Versions/3.5/bin/python3

import sys, os
sys.path.append('/Users/nick/Projects/LocalHackDay/localhackday/mysite')
os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'
from django.conf import settings
from django.core.mail import send_mail

def send_email():
    name, surname, email = sys.argv[1:]
    subject = 'Checkout new carpoolings!'
    message = 'Test email message:\n\n\tYou will be receiving this email every 5 minutes :P.\n\tEnjoy!'
    from_email = settings.EMAIL_HOST_USER
    to_list = [email,]
    send_mail(subject,message,from_email,to_list,fail_silently=False)

send_email()
